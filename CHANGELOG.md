# Change Log

All notable changes to the "mdoc-extension-pack" extension pack will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]
- No idea :-)

## [0.0.1] - 2019-09-25
### Added
- Extension "Markdown All in One"
- Extension "Markdown PDF"
- Extension "Markdown Preview Enhanced"
- Extension "Markdown Shortcuts"
- Extension "markdownlint"
- Extension "GitLens"
- Extension "Git History"
- Extension "Git Graph"
- Extension "NG PlantUML"