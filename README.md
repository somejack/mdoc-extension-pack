# MDoc Extension Pack

This extension pack packages the extensions needed for creating documentation in markdown.

## Extensions Included

* [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) - View git log, file or line historyAll you need to write Markdown (keyboard shortcuts, table of contents, auto preview and more.
* [Markdown PDF](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf) - Convert Markdown to PDF.
* [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced) - Markdown Preview Enhanced ported to vscode.
* [Markdown Shortcuts](https://marketplace.visualstudio.com/items?itemName=mdickin.markdown-shortcuts) - Shortcuts for Markdown editing.
* [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint) - Markdown linting and style checking.
* [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) - Supercharge the Git capabilities built into Visual Studio Code — Visualize code authorship at a glance via Git blame annotations and code lens, seamlessly navigate and explore Git repositories, gain valuable insights via powerful comparison commands, and so much more.
* [Git History](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory) - View git log, file history, compare branches or commits.
* [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph) - View a Git Graph of your repository, and perform Git actions from the graph.
* [NG PlantUML](https://marketplace.visualstudio.com/items?itemName=jkeys089.plantuml) - Preview & generate PlantUML diagrams.

## Want to see more extension added?

I'd be happy to take a look.

**Enjoy!**